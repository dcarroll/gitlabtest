public with sharing class DemoController {
    /**
     * An empty constructor for the testing
     */
    public DemoController() {
        System.debug('Yo, dooder');
    }

    /**
     * Get the version of the SFDX demo app
     * more comments
     */
    public String getAppVersion() {
        return '1.0.0';
    }
}
