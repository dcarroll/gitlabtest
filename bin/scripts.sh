#!/bin/bash

export SF_USERNAME=PROD
export DECRYPTION_KEY=3B94E5C86A9C61EC7E4274AD85819EA24F365109600F8B596C5380D3BCC8CBEE
export DECRYPTION_IV=CE1C55C9A90298B5FEEEACB4E3092DC0
export HUB_CONSUMER_KEY=3MVG9SemV5D80oBdzMjZjO_n1YrSsNaIUQg31bTdxuDRddUDey9RNU0KiK0rcFLiiKyPcWJ05mwTK1oLV1_pU
export HUB_SFDX_USER=dcarroll-bb15@force.com


function setuporg() {
    sfdx force:source:push
}

function runtests() {
    sfdx force:apex:test:run -c -d ~/junit -r junit --wait 5
}

function setup_sfdx () {

    # Decrypt server key
    openssl enc -nosalt -aes-256-cbc -d -in assets/server.key.enc -out assets/server.key -base64 -K $DECRYPTION_KEY -iv $DECRYPTION_IV

    # Install jq
    apt update && apt -y install jq

    # Setup SFDX environment variables
    export CLIURL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
    export SFDX_AUTOUPDATE_DISABLE=false
    export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
    export SFDX_DOMAIN_RETRY=300
    export SFDX_DISABLE_APP_HUB=true
    export SFDX_LOG_LEVEL=DEBUG
    export ROOTDIR=force-app/main/default/
    export TESTLEVEL=RunLocalTests
    export PACKAGEVERSION=""

    # Install Salesforce CLI
    mkdir sfdx
    wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
    "./sfdx/install"
    export PATH=./sfdx/$(pwd):$PATH
    #hack
    mkdir forcecli
    wget https://force-cli.heroku.com/releases/v0.26.1/linux-amd64/force
    cp force forcecli/force
    chmod +x forcecli/force
    export PATH=./forcecli:$PATH
    force -h
    #endhack
    # Output CLI version and plug-in information
    sfdx --version
    sfdx plugins --core

    # Authenticate using JWT
    sfdx force:auth:jwt:grant --clientid $HUB_CONSUMER_KEY --jwtkeyfile assets/server.key --username $HUB_SFDX_USER --setdefaultdevhubusername -a $SF_USERNAME
    #sfdx force:auth:jwt:grant --clientid $SF_CONSUMER_KEY --jwtkeyfile server.key --username $SF_USERNAME --setdefaultdevhubusername --setalias HubOrg
    #hack set force cli to same auth as hub
    force usedxauth $SF_USERNAME
    
   # Output CLI version and plug-in information
    sfdx --version
    sfdx plugins --core

    # Authenticate using JWT
    sfdx force:auth:jwt:grant --clientid $HUB_CONSUMER_KEY --jwtkeyfile assets/server.key --username $HUB_SFDX_USER --setdefaultdevhubusername -a $SF_USERNAME
    
    # hack set force cli to same auth as hub
    force usedxauth $SF_USERNAME
    
    echo "calling force field list ... "
    field=$(force field list ActiveScratchOrg | grep PipelineId__c)
    if  [ -z "$field" ]; then
      echo "creating new field in ActiveScratchOrg"
      force field create ActiveScratchOrg PipelineId:String unique:true
    fi
    echo "done checking for field..."
}

function create_scratch_org() {
    # This uses the pipeline ID for the scratch org username. This will allow us to reference the scratch org from a different job, ie to delete it
    output=$(sfdx force:org:create --setdefaultusername --definitionfile config/project-scratch-def.json --setalias review_$CI_PIPELINE_ID --targetdevhubusername $SF_USERNAME --wait 10 --durationdays 1 --json)
    # local output=$(sfdx force:org:create --targetdevhubusername $1 --setdefaultusername --definitionfile config/project-scratch-def.json --wait 10 --durationdays 1)
    scratch_org_username="$(jq -r '.result.username' <<< $output)"
    echo "Scratch org username is $scratch_org_username"
    echo $scratch_org_username > SCRATCH_ORG_USERNAME.txt
    cat SCRATCH_ORG_USERNAME.txt
    source bin/scripts.sh
    setuporg
    runtests
    sfdx force:org:open -r
  }